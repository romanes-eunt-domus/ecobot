import requests
import json
import schedule, time
import os
from prometheus_client import start_http_server, Gauge

url = os.environ.get('ECO_API_URL')
listen_port = os.environ.get('LISTEN_PORT', 80)
online_players_gauge = Gauge("online_players", "Online Players")
total_players_gauge = Gauge("total_players", "Total Players")


def update_gauges():
    resp = requests.get(url)
    stats = json.loads(resp.content)

    online_players_gauge.set(stats['OnlinePlayers'])
    total_players_gauge.set(stats['TotalPlayers'])


if __name__ == '__main__':
    schedule.every(1).seconds.do(update_gauges)
    start_http_server(8000)
    while True:
        schedule.run_pending()
        time.sleep(0.5)
